
#!/usr/bin/env python

'''
For cosine
'''

from __future__ import division
import matplotlib.pyplot as plt
import numpy as np


def factorial(n):
    assert(type(n) == int)
    assert(n >= 0)
    if n == 0:
        return 1
    else:
        fact = 1
        i = n
        while 1 <= i:
            fact *= i
            i -= 1
        return fact


def term(n, x):
    '''
    Nth term in exponential polynomial, 1/n! x^n
    '''
    N = 2*n + 1
    term = (1 / factorial(N)) * (x ** N) * ((-1) ** n)
    #print(n, N, x, term)
    return term


def nth_poly(n, x):
    '''
    value of the nth taylor polynomial (note 1 = 0th polynomial), evaluated at
    x
    '''
    assert(type(n) == int)
    assert(n >= 0)

    fx = 0
    i = 0
    while i <= n:
        fnx = term(i, x)
        fx += fnx
        i += 1
    return fx

if __name__ == '__main__':
    xmax = 10
    rangemin = -1*float(xmax)
    rangemax = float(xmax)
    xs = np.arange(rangemin, rangemax, 0.01)
    for n in range(20):
        N = n
        ys = [nth_poly(N, x) for x in xs]
        title = '$p_i(x)$, approximant %d, plotted from $x=%.1f$ to $x=%.1f$' % (N, rangemin, rangemax)
        filename = 'img_sin_nth%02d_pm%d.png' % (N, int(rangemax))
        print(filename, title)

        plt.plot(xs, ys, lw=2)
        plt.title(title)
        plt.ylim(-10, 10)
        plt.savefig(filename)
        #plt.show()
        plt.clf()

