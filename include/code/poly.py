#!/usr/bin/env python

from __future__ import division
import matplotlib.pyplot as plt
import numpy as np


def factorial(n):
    assert(type(n) == int)
    assert(n >= 0)
    if n == 0:
        return 1
    else:
        fact = 1
        i = n
        while 1 <= i:
            fact *= i
            i -= 1
        return fact


def term(n, x):
    '''
    Nth term in exponential polynomial, 1/n! x^n
    '''
    return (1 / factorial(n)) * (x ** n)


def nth_poly(n, x):
    '''
    value of the nth taylor polynomial (note 1 = 0th polynomial), evaluated at
    x
    '''
    assert(type(n) == int)
    assert(n >= 0)

    fx = 0
    i = 0
    while i <= n:
        fnx = term(i, x)
        fx += fnx
        i += 1
    return fx

if __name__ == '__main__':
    xs = np.arange(-10.0, 10.0, 0.01)
    for N in range(20):
        ys = [nth_poly(N, x) for x in xs]
        title = '%s-th recursive polynomial, plotted from $x=-10$ to $x=10$' % (N)
        filename = 'img_recurse%02d_pm10.png' % (N)
        print(filename, title)

        plt.plot(xs, ys, lw=2)
        plt.title(title)
        plt.ylim(-10, 10)
        plt.savefig(filename)
        plt.clf()

